const getFibonacci = require('./fibonacci');

describe('fibonacci', () => {
    it('should return 0 for input 0', () => {
        expect(getFibonacci(0)).toBe(0);
    });

    it('should return 1 for input 1', () => {
        expect(getFibonacci(1)).toBe(1);
    });
    it('should return the correct Fibonacci sequence for positive numbers', () => {
        expect(getFibonacci(2)).toBe(1);
        expect(getFibonacci(3)).toBe(2);
        expect(getFibonacci(4)).toBe(3);
    });
});