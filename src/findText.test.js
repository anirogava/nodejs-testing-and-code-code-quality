const findText = require('./findText');
const getFileContent = require('./getFileContent');

jest.mock('./getFileContent');

describe('findText', () => {
  it('should return true if text is found in content', async () => {
    getFileContent.mockResolvedValue('This is some sample text.');
    const result = await findText('sample.txt', 'sample');
    expect(result).toBe(true);
  });

  it('should return false if text is not found in content', async () => {
    getFileContent.mockResolvedValue('This is some sample text.');
    const result = await findText('sample.txt', 'missing');
    expect(result).toBe(false);
  });

  it('should reject with an error if getFileContent rejects', async () => {
    getFileContent.mockRejectedValue(new Error('File not found'));
    await expect(findText('nonexistent.txt', 'text')).rejects.toThrowError('File not found');
  });
});