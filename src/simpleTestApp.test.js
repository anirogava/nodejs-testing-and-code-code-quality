const startApp = require('./simpleTestApp');
const request = require('supertest');

let server;

beforeAll(async () => {
    server = await startApp();
});

afterAll(async () => {
    server.close();
});

describe('simpleTestApp', () => {
    it('should initially return an empty array of tasks', async () => {
        const response = await request(server).get('/');
        expect(response.status).toBe(200);
        expect(response.body).toEqual([]);
    });

    it('should add a task and return 200 when POSTing valid data', async () => {
        const newTask = { title: 'Task 1', description: 'Description for Task 1' };
        const response = await request(server)
            .post('/')
            .send(newTask);

        expect(response.status).toBe(200);

        const getResponse = await request(server).get('/');
        expect(getResponse.status).toBe(200);
        expect(getResponse.body).toEqual([newTask]);
    });

    it('should return 400 when POSTing invalid data', async () => {
        const invalidTask = { title: 'A', description: '' };
        const response = await request(server)
            .post('/')
            .send(invalidTask);

        expect(response.status).toBe(400);
    });
});
